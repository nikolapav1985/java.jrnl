/**
*
* Array class
*
* example of generic array method
*
* ----- compile -----
*
* javac Main.java
*
* ----- run -----
*
* java Main
*
*/
class Array{
    public static <E> void print(E[] arr){ // print array
        for(E elm:arr){
            System.out.printf("%s ",elm);
        }
    }
}
